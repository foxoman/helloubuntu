#!/usr/bin/python

# Ubuntu Tweak - PyGTK based desktop configuration tool
#
# Copyright (C) 2007-2008 TualatriX <tualatrix@gmail.com>
#
# Ubuntu Tweak is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Ubuntu Tweak is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Ubuntu Tweak; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA

import pygtk
pygtk.require("2.0")
import os
import gtk
import gtk.gdk
import gconf
import gobject
import logging

#CurrentScreenNum = gtk.gdk.display_get_default().get_default_screen().get_number()


log = logging.getLogger('compiz')


def load_ccm():
    global ccm
    try:
        import ccm
    except:
        log.error('No ccm available')
        pass

load_ccm()

plugins = \
{
    'expo': ('Show Workspaces'),
    'scale':('Show Windows'),
    'core': ('Show Desktop'),
    'widget': ('Show Widgets'),
}

plugins_settings = \
{
    'expo': 'expo_edge',
    'scale': 'initiate_all_edge',
    'core': 'show_desktop_edge',
    'widget': 'toggle_edge',
}

has_ccm = True
has_right_compiz = 1

class CompizPlugin:
    if has_ccm and has_right_compiz == 1:
        import compizconfig as ccs
        context = ccs.Context()
    elif has_right_compiz == 0:
        context = None
        error = False

    @classmethod
    def update_context(cls):
        if has_ccm and has_right_compiz == 1:
            import compizconfig as ccs
            load_ccm()
            cls.context = ccs.Context()

    @classmethod
    def set_plugin_active(cls, name, active):
        try:
            plugin = cls.context.Plugins[name]
            plugin.Enabled = int(active)
            cls.context.Write()
        except:
            pass

    @classmethod
    def get_plugin_active(cls, name):
        try:
            plugin = cls.context.Plugins[name]
            return bool(plugin.Enabled)
        except:
            return False

    def __init__(self, name):
        self.__plugin = self.context.Plugins[name]

    def set_enabled(self, bool):
        self.__plugin.Enabled = int(bool)
        self.save()

    def get_enabled(self):
        return self.__plugin.Enabled

    def save(self):
        self.context.Write()

    def resolve_conflict(self):
        conflicts = self.get_enabled() and self.__plugin.DisableConflicts \
                                       or self.__plugin.EnableConflicts
        conflict = ccm.PluginConflict(self.__plugin, conflicts)
        return conflict.Resolve()

    @classmethod
    def is_available(cls, name, setting):
        if ccm.Version >= '0.9.2':
            target = 'Screen'
        else:
            target = 'Display'

        return cls.context.Plugins.has_key(name) and \
                getattr(cls.context.Plugins[name], target).has_key(setting)

    def create_setting(self, key, target):
        settings = (self.__plugin, target)

        #TODO remove it in the future
        if ccm.Version >= '0.9.2':
            target = 'Screen'
        else:
            if not target:
                target = 'Display'

        settings = getattr(self.__plugin, target)

        if type(settings) == list:
            return settings[0][key]
        else:
            return settings[key]

class CompizSetting:
    def __init__(self, plugin, key, target=''):
        self.__plugin = plugin
        self.__setting = self.__plugin.create_setting(key, target)

    def set_value(self, value):
        self.__setting.Value = value
        self.__plugin.save()

    def get_value(self):
        return self.__setting.Value

    def is_default_and_enabled(self):
        return self.__setting.Value == self.__setting.DefaultValue and \
                self.__plugin.get_enabled()

    def reset(self):
        self.__setting.Reset()
        self.__plugin.save()

class OpacityMenu(gtk.CheckButton):
    menu_match = 'Tooltip | Menu | PopupMenu | DropdownMenu'

    def __init__(self, label):
        gtk.CheckButton.__init__(self, label)

        try:
            self.plugin = CompizPlugin('obs')
        except KeyError, e:
            log.error(e)
            self.set_sensitive(False)
        else:
            if not self.plugin.get_enabled():
                self.plugin.set_enabled(True)
            self.setting_matches = CompizSetting(self.plugin, 'opacity_matches', target='Screens')
            self.setting_values = CompizSetting(self.plugin, 'opacity_values', target='Screens')

            if self.menu_match in self.setting_matches.get_value():
                self.set_active(True)

            self.connect("toggled", self.on_button_toggled)

    def on_button_toggled(self, widget):
        if self.get_active():
            self.setting_matches.set_value([self.menu_match])
            self.setting_values.set_value([90])
        else:
            self.setting_matches.set_value([])
            self.setting_values.set_value([])

class WobblyMenu(gtk.CheckButton):
    def __init__(self, label, mediator):
        gtk.CheckButton.__init__(self, label)

        self.mediator = mediator
        try:
            self.plugin = CompizPlugin('wobbly')
        except KeyError, e:
            log.error(e)
            self.set_sensitive(False)
        else:
            self.map_window_setting = CompizSetting(self.plugin, 'map_window_match', target='Screens')
            self.map_effect_setting = CompizSetting(self.plugin, 'map_effect', target='Screens')

            if self.map_window_setting.is_default_and_enabled() and self.map_effect_setting.get_value() == 1:
                self.set_active(True)

            self.connect("toggled", self.on_button_toggled)

    def on_button_toggled(self, widget):
        if self.get_active():
            if self.plugin.resolve_conflict():
                self.mediator.snap.set_active(False)
                self.plugin.set_enabled(True)
                self.map_window_setting.reset()
                self.map_effect_setting.set_value(1)
        else:
            self.map_window_setting.set_value("")
            self.map_effect_setting.set_value(0)

        if self.map_window_setting.is_default_and_enabled():
            self.set_active(True)
        else:
            self.set_active(False)

class WobblyWindow(gtk.CheckButton):
    def __init__(self, label, mediator):
        gtk.CheckButton.__init__(self, label)

        self.mediator = mediator
        try:
            self.plugin = CompizPlugin('wobbly')
        except KeyError, e:
            log.error(e)
            self.set_sensitive(False)
        else:
            self.setting = CompizSetting(self.plugin, 'move_window_match', target='Screens')

            if self.setting.is_default_and_enabled():
                self.set_active(True)

            self.connect("toggled", self.on_button_toggled)

    def on_button_toggled(self, widget):
        if self.get_active():
            if self.plugin.resolve_conflict():
                self.mediator.snap.set_active(False)
                self.plugin.set_enabled(True)
                self.setting.reset()
        else:
            self.setting.set_value("")

        if self.setting.is_default_and_enabled():
            self.set_active(True)
        else:
            self.set_active(False)

class SnapWindow(gtk.CheckButton):
    def __init__(self, label, mediator):
        gtk.CheckButton.__init__(self, label)

        self.mediator = mediator
        try:
            self.plugin = CompizPlugin('snap')
        except KeyError, e:
            log.error(e)
            self.set_sensitive(False)
        else:
            self.set_active(self.plugin.get_enabled())
            self.connect("toggled", self.on_button_toggled)

    def on_button_toggled(self, widget):
        if self.get_active():
            if self.plugin.resolve_conflict():
                self.plugin.set_enabled(True)
                self.mediator.wobbly_w.set_active(False)
                self.mediator.wobbly_m.set_active(False)
        else:
            self.plugin.set_enabled(False)

class UnityLauncherAutoHide(gtk.CheckButton):
    def __init__(self, label):
        gtk.CheckButton.__init__(self, label)

        try:
            self.plugin = CompizPlugin('unityshell')
        except KeyError, e:
            log.error(e)
            self.set_sensitive(False)
        else:
            self.autohide_setting = CompizSetting(self.plugin, 'launcher_hide_mode')
            if self.autohide_setting.get_value()!=0:
              self.set_active(True)
            else:
              self.set_active(False)

            self.connect("toggled", self.on_button_toggled)

    def on_button_toggled(self, widget):
        if self.get_active():
            if self.plugin.resolve_conflict():
                self.autohide_setting.set_value(2)
        else:
            self.autohide_setting.set_value(0)

class ViewpointSwitcher(gtk.CheckButton):
    def __init__(self, label):
        gtk.CheckButton.__init__(self, label)

        try:
            self.plugin = CompizPlugin('vpswitch')
        except KeyError, e:
            log.error(e)
            self.set_sensitive(False)
        else:
            self.left_button_setting = CompizSetting(self.plugin, 'left_button')
            self.right_button_setting = CompizSetting(self.plugin, 'right_button')

            self.set_active(self.__get_setting_enabled())
            self.connect("toggled", self.on_button_toggled)

    def on_button_toggled(self, widget):
        if self.get_active():
            log.debug("The viewport button is enabled")
            if self.plugin.resolve_conflict():
                self.plugin.set_enabled(True)
                self.left_button_setting.set_value('Button4')
                self.right_button_setting.set_value('Button5')
        else:
            log.debug("The viewport button is disabled")
            self.plugin.set_enabled(False)

    def __get_setting_enabled(self):
        if self.plugin.get_enabled() and self.left_button_setting.get_value() == 'Button4' \
                and self.right_button_setting.get_value() == 'Button5':
                    return True
        else:
            return False


