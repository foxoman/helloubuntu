# -*- coding: utf-8 -*-
### BEGIN LICENSE
# # Copyright (C) 2011 <foxoman> <foxoman.u@gmail.com>
# This program is free software: you can redistribute it and/or modify it 
# under the terms of the GNU General Public License version 3, as published 
# by the Free Software Foundation.
# 
# This program is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranties of 
# MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR 
# PURPOSE.  See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along 
# with this program.  If not, see <http://www.gnu.org/licenses/>.
### END LICENSE

import sys
import os
import pygtk
pygtk.require ("2.0")
import gobject
gobject.threads_init()
import glib
import gtk
import vte
import subprocess
from subprocess import Popen, PIPE



from uhelper.uhelperconfig import getdatapath



class CopmizcheckDialog(gtk.Dialog):
	__gtype_name__ = "CopmizcheckDialog"

	def __init__(self):
		"""__init__ - This function is typically not called directly.
		Creation of a CopmizcheckDialog requires redeading the associated ui
		file and parsing the ui definition extrenally, 
		and then calling CopmizcheckDialog.finish_initializing().
	
		Use the convenience function NewCopmizcheckDialog to create 
		a CopmizcheckDialog object.
	
		"""
		pass

	def finish_initializing(self, builder):
		"""finish_initalizing should be called after parsing the ui definition
		and creating a CopmizcheckDialog object with it in order to finish
		initializing the start of the new CopmizcheckDialog instance.
	
		"""
		#get a reference to the builder and set up the signals
		self.builder = builder
		self.builder.connect_signals(self)
		
		self.terminal_scroll = self.builder.get_object("terminal_scroll")
		# set up terminal
		self.terminal = vte.Terminal()
		self.terminal_scroll.add(self.terminal)
		#self.terminal.fork_command()
		self.terminal.show()
		
        
	def run_ex_term(self, widget, data=None):
		term_title = 'فحص كرت الشاشة و الكومبيز'
		'''
		task = subprocess.Popen('gnome-terminal -e compiz-check-uhelper -t "%s"'% term_title, shell=True )
		if task.wait():
			raise CommandFailError
		'''
		p = subprocess.Popen(["gnome-terminal","-e", "/usr/lib/nux/unity_support_test -p" ,"-t", term_title])
		glib.timeout_add_seconds(1, lambda p: p.poll() == None, p)
		
		
	def run_term(self, widget, data=None):
		cmd = "/usr/lib/nux/unity_support_test -p"
		import commands
		res = commands.getoutput(cmd)
		print res
		self.builder.get_object("label2").set_label(res)
		#argv = [cmd, self.current_filename]
		#directory = os.path.split(self.current_filename)[0]
		#self.terminal.fork_command(command=cmd, argv=argv, envv=None, directory=directory)
		#self.terminal.fork_command(command=cmd, argv="p", envv=None, directory=None)
		self.terminal.set_data(res,None)
		#self.terminal_expander.set_expanded(True)
	def ok(self, widget, data=None):
		"""ok - The user has elected to save the changes.
		Called before the dialog returns gtk.RESONSE_OK from run().
	
		"""
		pass

	def cancel(self, widget, data=None):
		"""cancel - The user has elected cancel changes.
		Called before the dialog returns gtk.RESPONSE_CANCEL for run()
	
		"""         
		pass

def NewCopmizcheckDialog():
    """NewCopmizcheckDialog - returns a fully instantiated
    dialog-camel_case_nameDialog object. Use this function rather than
    creating CopmizcheckDialog instance directly.
    
    """

    #look for the ui file that describes the ui
    ui_filename = os.path.join(getdatapath(), 'ui', 'CopmizcheckDialog.ui')
    if not os.path.exists(ui_filename):
        ui_filename = None

    builder = gtk.Builder()
    builder.add_from_file(ui_filename)    
    dialog = builder.get_object("copmizcheck_dialog")
    dialog.finish_initializing(builder)
    return dialog

if __name__ == "__main__":
    dialog = NewCopmizcheckDialog()
    dialog.show()
    gtk.main()

