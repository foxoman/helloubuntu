# -*- coding: utf-8 -*-
### BEGIN LICENSE
# # Copyright (C) 2011 <foxoman> <foxoman.u@gmail.com>
# This program is free software: you can redistribute it and/or modify it 
# under the terms of the GNU General Public License version 3, as published 
# by the Free Software Foundation.
# 
# This program is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranties of 
# MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR 
# PURPOSE.  See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along 
# with this program.  If not, see <http://www.gnu.org/licenses/>.
### END LICENSE

import gtk

class SplashWindow(gtk.Window):
    def __init__(self):
        gtk.Window.__init__(self, gtk.WINDOW_POPUP)
        self.set_position(gtk.WIN_POS_CENTER)
        vbox = gtk.VBox(False, 0)
        image = gtk.Image()
        import os
        from uhelper.uhelperconfig import getdatapath
        DATA_DIR = getdatapath()
        image.set_from_file(os.path.join(DATA_DIR, 'media/splash.png'))

        vbox.pack_start(image)
        self.add(vbox)
