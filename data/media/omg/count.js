var DISQUSWIDGETS;

if (typeof DISQUSWIDGETS != 'undefined') {
    DISQUSWIDGETS.displayCount({"showReactions": true, "text": {"and": "and", "reactions": {"zero": "0 Reactions", "multiple": "{num} Reactions", "one": "1 Reaction"}, "comments": {"zero": "0 Comments", "multiple": "{num} Comments", "one": "1 Comment"}}, "counts": [{"reactions": 0, "uid": 1, "comments": 7}, {"reactions": 0, "uid": 0, "comments": 3}, {"reactions": 10, "uid": 3, "comments": 6}, {"reactions": 0, "uid": 2, "comments": 9}, {"reactions": 20, "uid": 5, "comments": 28}, {"reactions": 45, "uid": 4, "comments": 17}, {"reactions": 13, "uid": 7, "comments": 7}, {"reactions": 58, "uid": 6, "comments": 57}, {"reactions": 18, "uid": 8, "comments": 15}]});
}
